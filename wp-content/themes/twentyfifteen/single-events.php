
<?php

/*
 Template Name: Events Single
 */
get_header();

$page_title = $wp_query->post->post_title;
$page_slug = $wp_query->post->post_name;
// print_r($page_slug);die;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<link href="style_home.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="assets/classyBox/js/jquery.classybox.js"></script>
<script type="text/javascript" src="assets/classyBox/js/jwplayer.js"></script>
<script type="text/javascript" src="assets/classyBox/js/jwplayer.html5.js"></script>
<link rel="stylesheet" type="text/css" href="assets/classyBox/css/jquery.classybox.css" />
<!-- <link rel="stylesheet" href="assets/css/responsive-nav.css">
<script src="assets/js/responsive-nav.js"></script> -->


<!-- <link rel="stylesheet" type="text/css" href="assets/css/demo.css" /> -->
<!-- <link rel="stylesheet" type="text/css" href="assets/css/style.css" /> -->
<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->

<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;



$current_url = $_SERVER['REQUEST_URI'];
$current_url = explode('/', $current_url);
$current_url = $current_url[count($current_url) - 2];
$gallery_offset = intval($current_url);


if(empty($gallery_offset))
$gallery_offset = 1;

$current_page = $gallery_offset;
$gallery_offset = ($gallery_offset - 1) * 10;  

$page_title = get_the_title($page_id);
// print_r($title);die;

$all_images = array();
$all_images_2 = array();
// $album_images = get_attached_media( 'image', $gallery_offset);

$event_gallery = get_field('gallery', $page_id);
$event_gallery = $event_gallery[0]['ngg_id'];
global $nggdb;
$images = $nggdb -> get_gallery($event_gallery);
// print_r($images);die;
foreach ( $images as $image ) {
 $all_images_2[] = $image->imageURL;
}

// print_r($all_videos);die;

$args = array( 'numberposts' =>-1, 'post_type'=>'porticons', 'suppress_filters' => 0, 'order' => 'ASC');
$icons = get_posts( $args );
$icon_items = array();


foreach ( $icons as $post ) :   setup_postdata( $post );

$title = $post->post_name;
$icon_id = get_the_ID();

$marq_thumb['slug'] = $title;
$marq_thumb['image'] = wp_get_attachment_url( get_post_thumbnail_id($icon_id) );

endforeach;
wp_reset_postdata();


$args = array(
              'show_option_all'   => '',
              'show_option_none'  => '',
              'orderby'           => 'ID',
              'order'             => 'ASC',
              'show_count'        => 0,
              'hide_empty'        => 0,
              'hierarchical'      => 0,
              'depth'             => 1,
              'suppress_filters' => 0
              );

$all_categories = get_categories($args);
$cat_array = array();

foreach ($all_categories as $key => $value) {
  $cat_array[$value -> name]['name'] = $value -> slug;

  if($value -> slug == $marq_thumb['slug'])
  $cat_array[$value -> name]['icon'] = $marq_thumb['image']; 
}

// print_r($cat_array);die;
$args = array( 'numberposts' =>-1, 'post_type'=>'events', 'suppress_filters' => 0, 'order' => 'ASC');
$navs = get_posts( $args );
$events_items = array();


$all_cat_string = "";

foreach ( $navs as $post ) :   setup_postdata( $post );

$title = get_the_title();
$event_id = get_the_ID();


$thumb = wp_get_attachment_url( get_post_thumbnail_id($event_id) );

$category = get_the_terms( $event_id, 'category' );
$cat_name = $category[0] -> name;
$cat_slug = $category[0] -> slug;


$events_items[] = array('title' => $title, 'cat_name' => $cat_name, 'cat_slug' => $cat_slug, 'thumb' =>$thumb);

endforeach;
wp_reset_postdata();
// print_r($events_items);die;
$counter = 0;
$max = count($cat_array);

foreach ($cat_array as $key => $value) {
  
  $all_cat_string = $all_cat_string.".".$value['name'];
  $counter++;

  if($counter < $max)
  {
    $all_cat_string = $all_cat_string.", ";
  }
}
// print_r($all_cat_string);die;

$array_menu = wp_get_nav_menu_items("Main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title'] = $m->title;
        $menu[$counter]['url'] = $m->url;
    }

    $counter++;
}
// print_r($menu);

$events_all = count($all_images_2);
$max_pages = ceil($events_all/10);

// print_r($events_all);die;

$start_offset = $gallery_offset;
$end_offset = $current_page * 10;

// print_r($start_offset.'---'.$end_offset);
for ($i=$start_offset; $i < $end_offset ; $i++) { 
  
  if(!empty($all_images_2[$i]))
  $all_images[] = $all_images_2[$i];

}

$all_videos = array();
$videos = get_field('postvidoes', $post->ID);

foreach ( $videos as $video_single ) {

// print_r($video_single);die;
$title = $video_single -> post_title;
$video = $video_single -> post_content;

$id = explode('v=', $video);
if(isset($id[1]))
{
  $id = explode('"', $id[1]);
  $id = explode('&', $id[0]);
  $id = $id[0];

  $thumb = 'http://img.youtube.com/vi/'.$id.'/hqdefault.jpg';
  $video = 'http://www.youtube.com/watch?v='.$id;
}

else
{
  $id = explode('.com/', $video);
  $id = $id[1];

  $image_url = parse_url($video);
    
  $ch = curl_init('http://vimeo.com/api/v2/video/'.substr($image_url['path'], 1).'.php');
  curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
  $a = curl_exec($ch);
  $hash = unserialize($a);

  $thumb = $hash[0]["thumbnail_large"];
  $video = 'https://player.vimeo.com/video/'.$id;

}


$all_videos[] = array('title'=> $title, 'video'=> $video, 'thumb'=> $thumb);

}
wp_reset_postdata();

// print_r($all_images);die;
// print_r($all_videos);die;

?>


<link href="assets/css/port_single.css" rel="stylesheet" media="screen">

<link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="assets/js/html5lightbox/html5lightbox.js"></script>



<?php 
//include 'analyticstracking.php' 
?>
<?php 
//include 'mobile_nav.php' 
?>

<div class="main_div container">
  <div class="nav_bar_empty"></div>
  <?php 
  include 'nav_bar.php';
  ?>
  <div class="main_div_2 container">
    <div class="arrow-up-right"></div>
    <div class="arrow-up-left"></div>
    <div class="title_span_div">
      <span class="title_span"><?= $page_title; ?></span>
    </div>
    <div class="client_list_div container">

<!-- <a href="https://player.vimeo.com/video/229712439" class="html5lightbox" data-group="myvideo2" data-thumbnail="https://img.youtube.com/vi/_mMKNw_Mlxs/default.jpg">Open a Lightbox Gallery</a>
<a href="https://www.youtube.com/watch?v=pChmieCDWpo" class="html5lightbox" data-group="myvideo2" data-thumbnail="https://img.youtube.com/vi/pChmieCDWpo/default.jpg" >Fourth Video</a> -->

      <ul id="filters" class="clearfix">
        <?php
        if(!empty($all_images))
        {
          ?>
          <li class="active"><span class="filter photos_filter_2" fID="photos">Photos</span></li>
          <?php

          if(!empty($all_videos))
          {
            ?>
            <li><span class="filter videos_filter_2" fID="videos">Videos</span></li>
            <?php
          }
        }
        else
        {
          {
            ?>
            <li class="active"><span class="filter video_filter" fID="videos">Videos</span></li>
            <?php
          }
        }
        ?>
      </ul>
      <div id="portfoliolist" class="container videos_filter div_filters">
        <div class="gallery_row container gallery">
        <?php

        foreach ($all_videos as $key => $value) {

          ?>
          <p>
<!-- <a href="<?= $value['video'] ?>" class="html5lightbox" data-group="myvideo" data-thumbnail="https://img.youtube.com/vi/_mMKNw_Mlxs/default.jpg">
  test
</a>
<a href="<?= $value['video'] ?>" class="html5lightbox" data-group="myvideo" data-thumbnail="https://img.youtube.com/vi/_mMKNw_Mlxs/default.jpg">
  test
</a> -->
          <!-- <a class="vid_gallery fancybox.iframe" href="<?= $value['video'] ?>" rel="videoPopup" title="<?= $value['title'] ?>"> -->
          <a href="<?= $value['video'] ?>" class="html5lightbox" data-nativehtml5controls="true" data-group="myvideo" data-thumbnail="https://img.youtube.com/vi/_mMKNw_Mlxs/default.jpg">
            <div class="gallery_cols col-sm-4">
              <img src="<?= $value['thumb'] ?>"/>
              <div class="gallery_title">
                <span>
                  <?= $value['title'] ?>
                </span>
              </div>
              
            </div>
          </a>
          <?php
        }
        ?>
        </div>
      </div>
      <div id="portfoliolist" class="container photos_filter div_filters active_div">
        <ul id="gallery-container">

            <?php
            foreach ($all_images as $key => $value) {

              list($width, $height) = getimagesize($value);
              $flag ='';
              $image_class = 'landscape';
              if($height > $width)
              {
                $image_class = 'portrait';
                // $flag = 'portrait_div';
              }
              

              ?>
              <a class="fancybox thumbnail <?= $flag ?>" rel="group" href="<?= $value ?>" style="background-image: url(<?= $value ?>)">
              </a>
              <?php
            }
            ?>

          </ul>
          <?php

      $prev_url = get_site_url().'/events/'.$page_slug.'/'.($current_page - 1);
      $next_url = get_site_url().'/events/'.$page_slug.'/'.($current_page + 1);

      if($current_page != $max_pages)
      $next = '';
      else  
      $next = 'no_link'; 

      if($current_page != 1)
      $prev = '';
      else  
      $prev = 'no_link'; 

      ?>
      <div class="page_div">
        <a href="<?= $prev_url; ?>">
          <div class="links prev_link <?= $prev?>">
            <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
          </div>
        </a>
        <?php

        $start = ceil($current_page/5);

        if($start != 1)
        $start = (($start - 1) * 5) + 1;

        $end = $start + 5;

        for ($i = $start; $i < $end; $i++) { 
          
          $active = '';

          if($current_page == $i)
          $active = 'links_active';

          if($current_page == $max_pages)
          $next = 'no_link';

          $current_url = get_site_url().'/events/'.$page_slug.'/'.$i;
          ?>
          <a href="<?= $current_url; ?>" class="<?= $active; ?>" >
            <div class="links">
              <span><?= $i?></span>
            </div>
          </a>
        <?php

        if($i == $max_pages)  
        break;

        }
        

        ?>
        <a href="<?= $next_url; ?>">
          <div class="links next_link <?= $next?>">
            <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
          </div>
        </a>
      </div>
      </div>
    </div>
    <div class="arrow-up-right_footer"></div>
    <div class="arrow-up-left_footer"></div>
  </div>
  <div class="footer">
    <div class="footer_div_1">
      <span>follow us on..</span>
      <div class="footer_img_div">
        <img src="assets/img/yt_icon.png">
        <img src="assets/img/fb_icon.png">
        <img src="assets/img/insta_icon.png">
      </div>
    </div>
    <div class="footer_copy_div">
      <span>&copy; <?php echo date("Y"); ?> EVENT HOUSE. All Rights Reserved. Designed & Developed by Hug Digital</span>
    </div>
  </div>
</div>
<?php get_footer(); ?>
<script>

$( document ).ready(function() {


var viewportWidth = $(window).width();
    viewportWidth = (viewportWidth * 80) / 100;

    var viewportHeight = (viewportWidth * 9)/16;

  //   $(".vid_gallery").ClassyBox({
  //     width: viewportWidth,
  //     height: viewportHeight,
  //     social: false
  // });

$('.filter').on('click',function (e) {
      
      var fID = $(this).attr('fID');

      $('.active_div').removeClass('active_div');
      $('.'+fID+'_filter').addClass('active_div');

      $('.active').removeClass('active');
      $('.'+fID+'_filter_2').parent().addClass('active');
});

$(window).scroll(function() {

  var scroll = $(window).scrollTop();
  var slide_height = parseInt($('.slider_div').css('height'));
// console.log(scroll);
  if(scroll > 40)
  {

    $('.arrow-up-right').css('border-top-width', '0');
    $('.arrow-up-left').css('border-top-width', '0');

    setTimeout(function(){ 
    
    $('.nav_bar').addClass('nav_bar_slide');

    }, 500);

    

  }

  else
  {

    $('.arrow-up-right').css('border-top-width', '90px');
    $('.arrow-up-left').css('border-top-width', '90px');

    $('.nav_bar').removeClass('nav_bar_slide');
  } 
});


});

window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.nav_bar_empty').css('height', $('.nav_bar').css('height'));

 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);
 $('.arrow-up-right').css('border-top-width', '90px');
 $('.arrow-up-left').css('border-top-width', '90px');


 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);
 $('.arrow-up-right_footer').css('border-bottom-width', '90px');
 $('.arrow-up-left_footer').css('border-bottom-width', '90px');

 $(".fancybox").fancybox();

 var windowWidth = $(window).width();

 if(windowWidth > 767)
{
  var newHeight = parseInt($('.nav_bar').css('height'));
  $('.fake_head').css('height', newHeight);
}

else
{
  var newHeight = parseInt($('.mobile_nav').css('height'));
  $('.fake_head').css('height', newHeight);
}

$('#loading').fadeOut();

}

$(window).on('resize', function()
{

  var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);

 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);


 var viewportWidth = $(window).width();
  viewportWidth = (viewportWidth * 80) / 100;
  var viewportHeight = (viewportWidth * 9)/16;

    $(".vid_gallery").ClassyBox({
      width: viewportWidth,
      height: viewportHeight,
      social: false
  });

});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>