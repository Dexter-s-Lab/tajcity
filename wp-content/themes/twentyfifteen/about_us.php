
<?php

/*
 Template Name: About Us
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<link href="style_home.css" rel="stylesheet" media="screen">
<link href="assets/css/hover.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="assets/css/responsive-nav.css">
<script src="assets/js/responsive-nav.js"></script>

<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;

$portfolio = get_field('portfolio', get_the_ID());

$who_we_are = get_field('who_we_are', get_the_ID());
$who_we_are = explode("\n", $who_we_are);

$who_we_are_left = array();
$who_we_are_right = array();

foreach ($who_we_are as $key => $value) {
  
  if(!empty($value) && $key % 2 == 0)
  $who_we_are_left[] = $value;  

  if(!empty($value) && $key % 2 == 1)
  $who_we_are_right[] = $value; 
}

// print_r($who_we_are_right);die;

$array_menu = wp_get_nav_menu_items("Main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}
// print_r($menu);

$args = array( 'numberposts' =>-1, 'post_type'=>'event_types', 'suppress_filters' => 0, 'order' => 'ASC');
$types = get_posts( $args );
$types_items = array();
foreach ( $types as $post ) :   setup_postdata( $post );


$title_1 = get_field('title_1', get_the_ID());
$title_2 = get_field('title_2', get_the_ID());
$type_id = get_the_ID();
$desc = get_the_content();
$image = wp_get_attachment_url( get_post_thumbnail_id($type_id) );

$types_items[] = array("title_1" => $title_1, "title_2" => $title_2, "image" => $image, "desc" => $desc);

endforeach;
wp_reset_postdata();


?>


<link href="assets/css/about_us.css" rel="stylesheet" media="screen">

<?php 
//include 'analyticstracking.php' 
?>
<?php 
//include 'mobile_nav.php' 
?>

<div class="main_div container">
  <div class="nav_bar_empty"></div>
  <?php 
  include 'nav_bar.php';
  ?>
  <div class="main_div_2">
    <div class="arrow-up-right"></div>
    <div class="arrow-up-left"></div>
    <div class="title_span_div">
      <span class="title_span">About Us</span>
    </div>
    <div class="client_list_div">
      <div class="title_div_1">
        <span class="title_div_1_span_1">Who</span>
        <span class="title_div_1_span_2">We Are</span>
      </div>
      <div class="who_we_are_div container">
        <div class="who_we_are_left_div col-sm-6">
          <ul>
          <?php
          foreach ($who_we_are_left as $key => $value) {
            ?>
            <li><?= $value; ?></li>
            <?php
          }
          ?>
          </ul>
        </div>
        <div class="who_we_are_right_div col-sm-6">
          <ul>
          <?php
          foreach ($who_we_are_right as $key => $value) {
            ?>
            <li><?= $value; ?></li>
            <?php
          }
          ?>
        </ul>
        </div>
      </div>
      <div class="dl_port_div">
        <a href="<?= $portfolio;?>" download class="hvr-shutter-in-horizontal dl_port_div_span">
          <!-- <span class="dl_port_div_span">Download Portfolio</span> -->
          Download Portfolio
        </a>
      </div>
      <div class="title_div_2">
        <span class="title_div_2_span_1">What</span>
        <span class="title_div_2_span_2">We do</span>
      </div>
      <div class="title_div_3">
        <span class="title_div_3_span_1">A GENUINELY INTEGRATED</span>
        <span class="title_div_3_span_2">360</span>
        <span class="title_div_3_span_3">DEGREES SOLUTION</span>
      </div>
      <div class="logos_div">
        <?php
        foreach ($types_items as $key => $value) {
          
          $counter = $key + 1;
          $class = "";

          if($counter == 1)
          $class = "logos_div_inner_active";

          ?>

          <div class="logos_div_inner logos_div_inner_upper logo_inner_<?= $counter;?> <?= $class; ?>" itemNumber="<?= $counter; ?>">
            <?php 
            if(empty($value['title_1']))
            {
              ?>
              <span class="hidden_span">...</span>
              <?php
            }
              ?>

            <a href="" class="hvr-bounce-in dead_links">
              <img src="<?= $value['image'];?>">
            </a>

            <?php 
            if(empty($value['title_1']))
            {
              ?>
              <span class="hidden_span">...</span>
              <?php
            }
              ?>
            <?php 
            if(!empty($value['title_1']))
            {
              ?>
                <span><?= $value['title_1'];?></span>
                <span><?= $value['title_2'];?></span>
              <?php
            }
            ?>
          </div>

          <?php
        }
        ?>
      </div>
      <div class="sep_div">
        <!-- <img src="assets/img/sep_img.png"> -->
        <div class="arrowSeperator">
        </div>
      </div>
      
      <?php
        foreach ($types_items as $key => $value) {
          
          $counter = $key + 1;
          $class = "";

          if($counter == 1)
          $class = "final_div_active";

          ?>

          <div class="final_div <?= $class; ?> final_div_new_<?= $counter; ?>">
            <p class="final_div_2">
              <?= $value['desc']; ?>
            </p>
          </div>

          <?php
        }
        ?>
    </div>
    <div class="arrow-up-right_footer"></div>
    <div class="arrow-up-left_footer"></div>
  </div>

   <?php 
  include 'footer_custom.php';
  ?>
  
</div>
<?php get_footer(); ?>


<script>

$( document ).ready(function() {

  $('.item').on('click',function (e) {
      
      var itemCount = $(this).attr('itemCount');

      $('.active_cont').fadeOut('');
      $('.active_cont').removeClass('active_cont');
      
      $('.item_cont_' + itemCount).fadeIn('');
      $('.item_cont_' + itemCount).addClass('active_cont');

      
      $('.active_item').removeClass('active_item');
      $('.item_' + itemCount).addClass('active_item');


    });

   $('.logos_div_inner_upper').on('click',function (e) {
      
      $('.logos_div_inner_active').removeClass('logos_div_inner_active');
      $(this).addClass('logos_div_inner_active');

      var itemNumber = $(this).attr('itemNumber');
      $('.final_div_active').removeClass('final_div_active');
      $('.final_div_new_'+itemNumber).addClass('final_div_active');

    });

$(window).scroll(function() {

  var scroll = $(window).scrollTop();
  var slide_height = parseInt($('.slider_div').css('height'));

  if(scroll > 40)
  {

    $('.arrow-up-right').css('border-top-width', '0');
    $('.arrow-up-left').css('border-top-width', '0');


    $('.nav_bar').addClass('nav_bar_slide');

  }

  else
  {

    $('.arrow-up-right').css('border-top-width', '90px');
    $('.arrow-up-left').css('border-top-width', '90px');

    $('.nav_bar').removeClass('nav_bar_slide');
  } 
});


});

window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.nav_bar_empty').css('height', $('.nav_bar').css('height'));

 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);
 $('.arrow-up-right').css('border-top-width', '90px');
 $('.arrow-up-left').css('border-top-width', '90px');


 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);
 $('.arrow-up-right_footer').css('border-bottom-width', '90px');
 $('.arrow-up-left_footer').css('border-bottom-width', '90px');

 var windowWidth = $(window).width();

 if(windowWidth > 767)
{
  var newHeight = parseInt($('.nav_bar').css('height'));
  $('.fake_head').css('height', newHeight);
}

else
{
  var newHeight = parseInt($('.mobile_nav').css('height'));
  $('.fake_head').css('height', newHeight);
}

$('#loading').fadeOut();

}

$(window).on('resize', function()
{

  var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);

 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);


});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>