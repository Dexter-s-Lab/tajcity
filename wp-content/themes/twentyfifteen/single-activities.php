
<?php

/*
 Template Name: What To Do
 */

?>
<title>What to do in Marassi</title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<link href="style_home.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="assets/css/responsive-nav.css">
<script src="assets/js/responsive-nav.js"></script>

<?php include 'spinner.php'; ?>
<!-- <script src="assets/js/pace.min.js"></script> -->
<script data-pace-options='{ "ajax": false }' src='assets/js/pace.js'></script>

<script src="assets/js/jquery.dimensions.min.js"></script>
<script src="assets/js/jquery.viewport.mini.js"></script>
<link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js"></script>

<?php
get_header();

global $post;
$page_id =  $post->ID;
$page_cont = get_field('content', $page_id);
// print_r($page_cont);

/****************** Nav Items ******************/

$args = array( 'numberposts' =>-1, 'post_type'=>'nav_items', 'suppress_filters' => 0);
$navs = get_posts( $args );


foreach ( $navs as $post ) :   setup_postdata( $post );

$title = get_the_title();
$nav_id = get_the_ID();
$nav_icon = wp_get_attachment_url( get_post_thumbnail_id($nav_id) );
$order = get_field('order', get_the_ID());
$link = get_field('link', get_the_ID());

$nav_items[$order] = array('title'=> $title, 'icon'=> $nav_icon, 'link'=> $link);

endforeach;
wp_reset_postdata();

ksort($nav_items);
// print_r($nav_items);die;

/**********************************************/


/****************** Slider Items ******************/

$args = array( 'numberposts' =>-1, 'post_type'=>'activities', 'suppress_filters' => 0);
$items = get_posts( $args );
$counter = 0;

foreach ( $items as $post ) :   setup_postdata( $post );

$title = get_the_title();
$post_name = $post->post_name;
$logo = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
$content = get_field('content', get_the_ID());
$activities_list = get_field('single_activities', get_the_ID());
// print_r($title."\r\n");
// print_r($activities_list);
// print_r('---------'."\r\n");
// $categories = wp_get_post_categories(get_the_ID());

$category_list = array();
$activities = array();
$single_act = array();

if (!empty($activities_list)) 
{
  foreach ($activities_list as $key => $value) {
    
    $categories = get_the_category($value -> ID);
    $act_title = $value -> post_title;
    $act_img = wp_get_attachment_url( get_post_thumbnail_id($value -> ID));
    $act_reg = get_field('registration', $value -> ID);
    $act_cont = get_field('content', $value -> ID);
    // print_r($act_cont);die;
    
    if(empty($categories))
    $activities[] = array('title'=> $act_title, 'img'=> $act_img, 'reg'=> $act_reg, 'content'=> $act_cont);  

    foreach ($categories as $key2 => $value2) {
      
      $activities[$value2 -> slug][] = array('title'=> $act_title, 'img'=> $act_img, 'reg'=> $act_reg, 'content'=> $act_cont);
      // $category_list[$value2 -> slug]['name'] = $value2 -> slug;

     $thumb = get_term_thumbnail( $value2 -> term_taxonomy_id, $size = ‘post-thumbnail’, $attr = ” );

     $pattern = '/src=[\'"]?([^\'" >]+)[\'" >]/';
    preg_match($pattern, $thumb, $link);
    $link = $link[1];
    $thumb = urldecode($link);

    $category_list[$value2 -> slug]['name'] = $value2 -> name;
    $category_list[$value2 -> slug]['thumb'] = $thumb;

    }
    
  }
}
// $link = get_permalink(get_the_ID());
$active = 0;
if($page_id == get_the_ID())
$active = 1;

$cat_flag = 1;
if(empty($categories))
$cat_flag = 0;

$items_list[$active][] = array('title'=> $title,'name'=> $post_name, 'logo'=> $logo, 'content'=> $content, 'counter'=> $counter
  , 'activities'=> $activities, 'categories'=> $category_list, 'cat_flag'=> $cat_flag);

$counter++;

endforeach;
wp_reset_postdata();


$items_list[1] = $items_list[1][0];
// ksort($items_list);
// print_r($items_list[1]);die;

/**********************************************/


?>

<link href="assets/css/single_activity.css" rel="stylesheet" media="screen">

<?php include 'modal_act.php' ?>

<?php include 'mobile_nav.php' ?>

<div class="md-overlay"></div>
  <div class="main_container" id="home">
    <div class="fake_nav"></div>
    <div class="top_div">
      <img src="assets/img/slide_3.jpg">
      </div>

      <?php include 'nav_bar.php' ?>

      <div class="white_strip">
      </div>

      <div class="orange_strip strips">
        <span>What to do in Marassi</span>
      </div>

      <div class="what_div container">
        <div class="what_slider">
            <div class="owl_slider container">
              <div class="left_rect_arrow prev_image">
                <div class="left_rect_arrow_div">
                  <span><</span>
                </div>
              </div>
              <div id="owl-demo" class="owl-carousel owl-theme owl-demo">
                <div class="item" itemCount="<?= $items_list[1]['counter']?>" itemName="<?= $items_list[1]['name']?>">
                  <img src="<?= $items_list[1]['logo']?>" class="item_<?= $items_list[1]['counter']?> active_item"/>
                  <span><?= $items_list[1]['title'] ?></span>
                </div>

                <?php 
                foreach ($items_list[0] as $key => $value) {
                  
                  ?>
                      <div class="item" itemCount="<?= $value['counter']?>" itemName="<?= $value['name']?>">
                        <img src="<?= $value['logo']?>" class = "item_<?= $value['counter']?>"/>
                        <span><?= $value['title']; ?></span>
                      </div>
                    <?php
                }
                ?>
              </div>
              <div class="right_rect_arrow next_image">
                <div class="right_rect_arrow_div">
                  <span>></span>
                </div>
              </div>
            </div>
              <div class="item_cont active_cont item_cont_<?= $items_list[1]['counter']?>">
              <?= $items_list[1]['content']; ?>
              <?php

              $count = 0;
              $count2 = 0;

              if($items_list[1]['cat_flag'])
              {
                foreach ($items_list[1]['categories'] as $key2 => $value2) {

                ?>
                <div class="single_div active_div cat_divs cat_div_<?= $items_list[1]['name']; ?>" catSlug="<?= $key2; ?>_<?= $items_list[1]['name']; ?>">
                  <div class="single_left single_divs">
                    <span><?= $value2['name']; ?></span>
                    <div class="act_img_div" aImg="<?= $value2['thumb'] ?>" style="background-image: url(<?= $value2['thumb']; ?>)"></div>
                  </div>
                </div>
                <?php

                if($count == 2)
                {
                  ?>
                  <div class="container"></div>
                  <?php
                  $count = 0;
                }
                else
                $count++;  

              foreach ($items_list[1]['activities'] as $key3 => $value3) {

                if($key3 == $key2)
                {
                  foreach ($value3 as $key4 => $value4) {
                    ?>
                    <div class="single_div cat_dir cat_<?= $key3; ?>_<?= $items_list[1]['name']; ?>">
                      <div class="single_left single_divs">
                        <span><?= $value4['title']; ?></span>
                        <div class="act_img_div" aImg="<?= $value4['img'] ?>" style="background-image: url(<?= $value4['img'] ?>)"></div>
                        <div style="display:none;" class="item_text">
                          <?= $value4['content']?>
                        </div>
                        <?php

                        if(!empty($value4['content']))
                        {
                          ?>
                          <button type="button" class="btn btn-default more_btn">More Info</button>
                        <?php
                        }

                        if($value4['reg'] == 'Yes')
                        {
                          ?>
                          <button type="button" class="btn btn-default reg_btn">Register Now</button>
                        <?php
                        }
                        ?>
                      </div>
                    </div>
                    <?php

                    if($count2 == 2)
                  {
                    ?>
                    <!-- <div class="container"></div> -->
                    <?php
                    $count2 = 0;
                  }
                  else
                  $count2++;
                  }

                }
              }

              }

              }
              else
              {
                // print_r($items_list[1]['activities']);
                foreach ($items_list[1]['activities'] as $key3 => $value3) {

                  $count2 = 0;

                    ?>
                    <div class="single_div dirs dir_<?= $items_list[1]['counter']?>  cat_dir cat_0_<?= $items_list[1]['name']; ?>">
                      <div class="single_left single_divs">
                        <span><?= $value3['title']; ?></span>
                        <div class="act_img_div" aImg="<?= $value3['img'] ?>" style="background-image: url(<?= $value3['img'] ?>)"></div>
                        <div style="display:none;" class="item_text">
                          <?= $value3['content']?>
                        </div>
                        <?php

                        if(!empty($value3['content']))
                        {
                          ?>
                          <button type="button" class="btn btn-default more_btn">More Info</button>
                        <?php
                        }

                        if($value3['reg'] == 'Yes')
                        {
                          ?>
                          <button type="button" class="btn btn-default reg_btn">Register Now</button>
                        <?php
                        }
                        ?>
                      </div>
                    </div>
                    <?php

                    if($count2 == 2)
                  {
                    ?>
                    <div class="container"></div>
                    <?php
                    $count2 = 0;
                  }
                  else
                  $count2++;
              }
              }

              ?>
              </div>
              <?php 

                foreach ($items_list[0] as $key => $value) {

                  $count = 0;
                  $count2 = 0;

                  ?>
                      <div class="item_cont <?= $active_cont; ?> item_cont_<?= $value['counter']?>">
                    <?= $value['content']; ?>
                    <?php

                if($value['cat_flag'])
                {
                  foreach ($value['categories'] as $key2 => $value2) {

                ?>
                <div class="single_div cat_divs cat_div_<?= $value['name']; ?>" catSlug="<?= $key2; ?>_<?= $value['name']; ?>">
                  <div class="single_left single_divs">
                    <span><?= $value2['name']; ?></span>
                    <div class="act_img_div" aImg="<?= $value2['thumb'] ?>" style="background-image: url(<?= $value2['thumb']; ?>)"></div>
                  </div>
                </div>
                <?php

                if($count == 2)
                {
                  ?>
                  <div class="container"></div>
                  <?php
                  $count = 0;
                }
                else
                $count++;  

              foreach ($value['activities'] as $key3 => $value3) {

                if($key3 == $key2)
                {
                  foreach ($value3 as $key4 => $value4) {
                    ?>
                    <div class="single_div cat_dir cat_<?= $key3; ?>_<?= $value['name']; ?>">
                      <div class="single_left single_divs">
                        <span><?= $value4['title']; ?></span>
                        <div class="act_img_div" aImg="<?= $value4['img'] ?>" style="background-image: url(<?= $value4['img'] ?>)"></div>
                        <div style="display:none;" class="item_text">
                          <?= $value4['content']?>
                        </div>
                        <?php

                        if(!empty($value4['content']))
                        {
                          ?>
                          <button type="button" class="btn btn-default more_btn">More Info</button>
                        <?php
                        }

                        if($value4['reg'] == 'Yes')
                        {
                          ?>
                          <button type="button" class="btn btn-default reg_btn">Register Now</button>
                        <?php
                        }
                        ?>
                      </div>
                    </div>
                    <?php

                    if($count2 == 2)
                  {
                    ?>
                    <!-- <div class="container"></div> -->
                    <?php
                    $count2 = 0;
                  }
                  else
                  $count2++;
                  }

                }
              }

              }
                }   

                else
              {
                // print_r('expression');
                foreach ($value['activities'] as $key3 => $value3) {

                  $count2 = 0;

                    ?>
                    <div class="single_div dirs dir_<?= $value['counter']?> cat_dir cat_<?= $value['counter']?>_<?= $value['name']; ?>">
                      <div class="single_left single_divs">
                        <span><?= $value3['title']; ?></span>
                        <div class="act_img_div" aImg="<?= $value3['img'] ?>" style="background-image: url(<?= $value3['img'] ?>)"></div>
                        <div style="display:none;" class="item_text">
                          <?= $value3['content']?>
                        </div>
                        <?php

                        if(!empty($value3['content']))
                        {
                          ?>
                          <button type="button" class="btn btn-default more_btn">More Info</button>
                        <?php
                        }

                        if($value3['reg'] == 'Yes')
                        {
                          ?>
                          <button type="button" class="btn btn-default reg_btn">Register Now</button>
                        <?php
                        }
                        ?>
                      </div>
                    </div>
                    <?php

                    if($count2 == 2)
                  {
                    ?>
                    <div class="container"></div>
                    <?php
                    $count2 = 0;
                  }
                  else
                  $count2++;
              }
              }
                

              ?>
            </div>
              <?php
                }
                ?>
            </div>  
      </div>

      <div class="footer_bg">
        <img src="assets/img/footer_bg.jpg">
      </div>

      <div class="white_strip">
      </div>

      <?php include 'new_footer.php' ?>

  </div>
    
<?php get_footer(); ?>

<?php
gravity_form(4, $display_title=true, $display_description=false, $display_inactive=false, $field_values=null, $ajax=true, 1);
?>

<script>

$( document ).ready(function() {

$('.active_cont .dirs').show();

var owl = $(".owl-demo");
 
  owl.owlCarousel({
     
     // items : 5,
      items :5, //10 items above 1000px browser width
      itemsDesktop : [1000,5], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
      navigation : true,
      scrollPerPage : true
 
  });

  $('.next_image').on('click',function (e) {
      $('.owl-next').trigger('touchend');
    });

    $('.prev_image').on('click',function (e) {
      $('.owl-prev').trigger('touchend');
    });

  $('.cat_divs').on('click',function (e) {
      
      $('.cat_divs').hide();
      var slug = $(this).attr('catSlug');
      $('.cat_' + slug).fadeIn();

    });

  $('.item').on('click',function (e) {
      
      var itemCount = $(this).attr('itemCount');

      if($('.item_cont_' + itemCount).hasClass('active_cont'))
      {

      }
      else
      {
        $('.active_cont').fadeOut('');
        $('.active_cont').removeClass('active_cont');
        
        $('.item_cont_' + itemCount).fadeIn('');
        $('.item_cont_' + itemCount).addClass('active_cont');

        
        $('.active_item').removeClass('active_item');
        $('.item_' + itemCount).addClass('active_item');
      }

      var itemName = $(this).attr('itemName');
      $('.cat_divs').hide();
      $('.cat_dir').hide();

      $('.cat_div_' + itemName).fadeIn();
      $('.dir_' + itemCount).fadeIn();


    });

  $('.more_btn').on('click',function (e) {

      e.preventDefault();

      var more_title = $(this).parent().find('span').html();
      var more_img = $(this).parent().find('.act_img_div').attr('aImg');
      var more_text = $(this).parent().find('.item_text').html();

      $('.more_title').html(more_title);
      $('.more_img').attr('src', more_img);
      $('.more_text').html(more_text);

      $('.more_info_mod').trigger('click');
      $('.md-close').fadeIn(1800);

    });

  $('.reg_btn').on('click',function (e) {

      e.preventDefault();

      $('.md_trigger_form').trigger('click');
      $('.md-close').fadeIn(1800);

    });

  $( ".act_form" ).submit(function( event ) {
    
    event.preventDefault();

    var n1 = $('.mob_input_1 input').val();
    var n2 = $('.mob_input_2 input').val();
    var n3 = $('.mob_input_3 input').val();
    var custom_number = n1 + '-' + n2 + '-' + n3;


    $('.custom_number').val(custom_number);
    
    $('#modal-2 .md-close').trigger('click');

    $('.md_trigger_sending').trigger('click');

    var formID = $(this).attr('formID');
    var inputs = $(this).find('.form_inputs');

    $.each( inputs, function( key, value ) {
      
      inputID = $(value).attr('inputID');
      var input_old = $(value).val();
      $('#input_' + formID + '_' + inputID).val(input_old);

    });


    var clone = $('#gform_' + formID).clone();
    $('#gform_' + formID).submit();
    $('.footer_div').append(clone);

    
    
    jQuery(document).bind('gform_post_render', function(){
   
    $('#modal-3 .md-close').trigger('click');

    setTimeout(function(){
      $('.md_trigger_success').trigger('click');
    }, 500);

    setTimeout(function(){
      $('#modal-4 .md-close').trigger('click');
    }, 1500);

    $.each( inputs, function( key, value ) {
      
      $(value).val('');

    });

  });

  });


});

window.onload = function() {

  var slider_height = $('.item img').css('height');
  $('.left_rect_arrow').css('height', slider_height);
  $('.right_rect_arrow').css('height', slider_height);

  var nav_height = $('.mobile_nav').css('height');
  $('.fake_nav').css('height', nav_height);

  $('.logo_div').fadeOut();
  $('.main_container').css('opacity', 1);

}

$(window).on('resize', function()
{

  var slider_height = $('.item img').css('height');
  $('.left_rect_arrow').css('height', slider_height);
  $('.right_rect_arrow').css('height', slider_height);

});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>