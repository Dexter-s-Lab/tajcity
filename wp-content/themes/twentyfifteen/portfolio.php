
<?php

/*
 Template Name: Portfolio
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<link href="style_home.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="assets/css/responsive-nav.css">
<script src="assets/js/responsive-nav.js"></script>

<link rel="stylesheet" href="assets/css/normalize.css">
<link rel="stylesheet" href="assets/css/layout.css">

<script type="text/javascript" src="assets/js/jquery.mixitup.min.js"></script>

<!-- <link rel="stylesheet" type="text/css" href="assets/css/demo.css" /> -->
<link rel="stylesheet" type="text/css" href="assets/css/style.css" />
<script src="assets/js/modernizr.custom.97074.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="assets/css/noJS.css"/></noscript>
<script type="text/javascript" src="assets/js/jquery.hoverdir.js"></script>
<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->

<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;

// print_r($cat_array);die;
$args = array( 'numberposts' =>-1, 'post_type'=>'porticons', 'suppress_filters' => 0, 'order' => 'ASC');
$icons = get_posts( $args );
$icon_items = array();


foreach ( $icons as $post ) :   setup_postdata( $post );

$title = $post->post_name;
$icon_id = get_the_ID();

$marq_thumb['slug'] = $title;
$marq_thumb['image'] = wp_get_attachment_url( get_post_thumbnail_id($icon_id) );

endforeach;
wp_reset_postdata();


$args = array(
              'show_option_all'   => '',
              'show_option_none'  => '',
              'orderby'           => 'ID',
              'order'             => 'ASC',
              'show_count'        => 0,
              'hide_empty'        => 0,
              'hierarchical'      => 0,
              'depth'             => 1,
              'suppress_filters' => 0
              );

$all_categories = get_categories($args);
$cat_array = array();

foreach ($all_categories as $key => $value) {
  $cat_array[$value -> name]['name'] = $value -> slug;

  if($value -> slug == $marq_thumb['slug'])
  $cat_array[$value -> name]['icon'] = $marq_thumb['image']; 
}

// print_r($cat_array);die;
$args = array( 'numberposts' =>-1, 'post_type'=>'events', 'suppress_filters' => 0, 'order' => 'ASC');
$navs = get_posts( $args );
$events_items = array();
$link_page = get_permalink();

$all_cat_string = "";

foreach ( $navs as $post ) :   setup_postdata( $post );

$title = get_the_title();
$event_id = get_the_ID();
$post_slug = $post->post_name;
// print_r($post_slug);die;


$thumb = wp_get_attachment_url( get_post_thumbnail_id($event_id) );

$category = get_the_terms( $event_id, 'category' );
$cat_name = $category[0] -> name;
$cat_slug = $category[0] -> slug;
// $cat_array[$cat_name]['name'] = $cat_slug;


// $cat_array[$cat_name]['icon'] = $cat_icon;

$link = get_site_url().'/events/'.$post_slug;

// $nav_id = get_the_ID();
// $image = wp_get_attachment_url( get_post_thumbnail_id($nav_id) );

$events_items[] = array('title' => $title, 'cat_name' => $cat_name, 'cat_slug' => $cat_slug, 'thumb' =>$thumb
  , 'link' =>$link);

endforeach;
wp_reset_postdata();
// print_r($events_items);die;
$counter = 0;
$max = count($cat_array);

foreach ($cat_array as $key => $value) {
  
  $all_cat_string = $all_cat_string.".".$value['name'];
  $counter++;

  if($counter < $max)
  {
    $all_cat_string = $all_cat_string.", ";
  }
}
// print_r($all_cat_string);die;

$array_menu = wp_get_nav_menu_items("Main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title'] = $m->title;
        $menu[$counter]['url'] = $m->url;
    }

    $counter++;
}
// print_r($menu);

?>


<link href="assets/css/portfolio.css" rel="stylesheet" media="screen">

<?php 
//include 'analyticstracking.php' 
?>
<?php 
//include 'mobile_nav.php' 
?>

<div class="main_div container">
  <div class="nav_bar_empty"></div>
  <?php 
  include 'nav_bar.php';
  ?>
  <div class="main_div_2 container">
    <div class="arrow-up-right"></div>
    <div class="arrow-up-left"></div>
    <div class="title_span_div">
      <span class="title_span">Portfolio</span>
    </div>
    <div class="client_list_div container">
      <ul id="filters" class="clearfix">
        <li><span class="filter all_filters" data-filter="<?= $all_cat_string; ?>">Show All</span></li>
        <?php
        foreach ($cat_array as $key => $value) {
          if(isset($value['icon']))
          {
            ?>
            <li><span class="filter marq_filter" data-filter=".<?= $value['name']; ?>"><img class="marq_img" src="<?= $value['icon'];?>"></span></li>
            <?php
          }
          else
          {
            ?>
            <li><span class="filter" data-filter=".<?= $value['name']; ?>"><?= $key; ?></span></li>
            <?php
          }
          ?>
          <?php
        }
        ?>
      </ul>
      <div id="portfoliolist" class="da-thumbs container">
      <?php
      foreach ($events_items as $key => $value) {
        // echo $key;
        ?>
          <div class="portfolio <?= $value['cat_slug'];?>" data-cat="<?= $value['cat_slug'];?>">
            <a href="<?= $value['link'];?>">
              <div class="portfolio-wrapper">       
                <img src="<?= $value['thumb'];?>" alt="" />
                <div class="label">
                  <div class="label-text">
                    <span class="text-title"><?= $value['title'];?></span>
                  </div>
                  <div class="label-bg"></div>
                </div>
              </div>
            </a>
          </div>
        <?php
      }
      ?>
    </div>
    </div>
    <div class="arrow-up-right_footer"></div>
    <div class="arrow-up-left_footer"></div>
  </div>
  
   <?php 
  include 'footer_custom.php';
  ?>
  
</div>
<?php get_footer(); ?>
<script>

$( document ).ready(function() {

  $('.portfolio-wrapper').on('click',function (e) {
      

    });

$(window).scroll(function() {

  var scroll = $(window).scrollTop();
  var slide_height = parseInt($('.slider_div').css('height'));
// console.log(scroll);
  if(scroll > 40)
  {

    $('.arrow-up-right').css('border-top-width', '0');
    $('.arrow-up-left').css('border-top-width', '0');

    setTimeout(function(){ 
    
    $('.nav_bar').addClass('nav_bar_slide');

    }, 500);

    

  }

  else
  {

    $('.arrow-up-right').css('border-top-width', '90px');
    $('.arrow-up-left').css('border-top-width', '90px');

    $('.nav_bar').removeClass('nav_bar_slide');
  } 
});


});

window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.nav_bar_empty').css('height', $('.nav_bar').css('height'));

 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);
 $('.arrow-up-right').css('border-top-width', '90px');
 $('.arrow-up-left').css('border-top-width', '90px');


 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);
 $('.arrow-up-right_footer').css('border-bottom-width', '90px');
 $('.arrow-up-left_footer').css('border-bottom-width', '90px');

 $(function () {
    
    var filterList = {
    
      init: function () {
      
        // MixItUp plugin
        // http://mixitup.io
        $('#portfoliolist').mixItUp({
          selectors: {
            target: '.portfolio',
            filter: '.filter' 
          },
          load: {
            // filter: '.all_filters'  
          }     
        });               
      
      }

    };
    
    // Run the show!
    filterList.init();
    $('.all_filters').addClass('active');
    
    
  }); 

 $(' .da-thumbs > .portfolio ').each( function() { $(this).hoverdir(); } );

 var windowWidth = $(window).width();

 if(windowWidth > 767)
{
  var newHeight = parseInt($('.nav_bar').css('height'));
  $('.fake_head').css('height', newHeight);
}

else
{
  var newHeight = parseInt($('.mobile_nav').css('height'));
  $('.fake_head').css('height', newHeight);
}

$('#loading').fadeOut();

}

$(window).on('resize', function()
{

  var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);

 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);

});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>