<nav class="navbar navbar-default navbar-fixed-top header_div">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" slideID="home">
              <img src="<?= $logo; ?>" class="menu_span" slideID="home">
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <?php 

              foreach ($menu as $key => $value) {
                ?>
                <li class="menu_span" slideID="<?= $value['url'];?>">
                  <a href="#"><?= $value['title'];?></a>
                </li>
                <?php
              }
              ?>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>