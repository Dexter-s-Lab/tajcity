<?php

/*
 Template Name: Thank You
 */

$page_title = $wp_query->post->post_title;

$logo = get_field('logo', get_the_ID());
$bg = get_field('bg', get_the_ID());

$array_menu = wp_get_nav_menu_items("home");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}

?>
<head>
<title><?= $page_title; ?></title>

<meta name="viewport" content="width=device-width,initial-scale=1.0">

<base href="<?php bloginfo('template_directory');?>/twentyseventeen"/>

<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

<link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="assets/css/style.css" rel="stylesheet" media="screen">

<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '135424837114170');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=135424837114170&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->


<script>
  fbq('track', 'Lead');
</script>

<!-- Global site tag (gtag.js) - Google AdWords: 869534557 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-869534557"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-869534557');
</script>
</style>
<script>(function(E,n,G,A,g,Y,a){E['EngLandingObject']=g;E[g]=E[g]||function(){ (E[g].q=E[g].q||[]).push(arguments)},E[g].l=1*new Date();Y=n.createElement(G), a=n.getElementsByTagName(G)[0];Y.async=1;Y.src=A;a.parentNode.insertBefore(Y,a)})(window,document,'script','//widget.engageya.com/eng_landing.js','__engLanding');__engLanding('createPixel',{pixelid:185463,dtms:0});</script>

<!-- Event snippet for Taj City conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-869534557/QcbyCMLw6nUQ3ZbQngM'});
</script>

</head>


<div class="main_div">

  <?php 
  //include 'nav_bar.php';
  ?>
  <div class="thank_bg">
    <img src="http://tajcity.mnhd.com/wp-content/uploads/2017/11/contact_bg.jpg">
    <div class="thank_you_div_2">
      <div class="thank_you_div_inner_2">
        <span>
          Thank you for registering!
        </span>
        <span>
          We will contact you soon.
        </span>
      </div>
    </div>
  </div>
  <div class="footer container">
    <div class="footer_left col-sm-6">
      <span class="footer_left_span_1 footer_left_spans">A project by:</span>
      <span class="footer_left_span_2 footer_left_spans">Madinet Nasr for Housing & Development (MNHD)</span>
      <span class="footer_left_span_3 footer_left_spans">© Copyright Madinet Nasr . All Rights Reserved designed and developed by <a href="https://hugdigital.com/" target="_blank">Hug Digital</a></span>
    </div>
    <div class="footer_right col-sm-6">
      <div class="footer_right_top">
        <a href="tel:16750">
          <span class="footer_right_span">16750</span>
        </a>
        <a href="https://www.facebook.com/TajCityOfficial/" target="_blank">
          <img class="idle_img" src="assets/img/fb_icon.png">
        </a>
        <a href="https://www.instagram.com/tajcity_official/" target="_blank">
          <img class="idle_img" src="assets/img/insta_icon.png">
        </a>
      </div>
      
    </div>
  </div>
</div>

<?php 
  include 'footer_div.php';
  ?>

<?php get_footer(); ?>

<script>

$( document ).ready(function() {


});

$(window).scroll(function() {



});

window.onload = function() {


$('.footer_right').css('height', $('.footer_left').css('height'));

}

$(window).on('resize', function()
{


});

</script>
<style type="text/css">
html
{
  margin-top: 0 !important;
}