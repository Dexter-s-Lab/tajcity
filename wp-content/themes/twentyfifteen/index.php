<?php

/*
 Template Name: Home
 */

$page_title = $wp_query->post->post_title;


$logo = get_field('logo', get_the_ID());
$slider_image = get_field('slider_image', get_the_ID());
$block_1_bg = get_field('block_1_bg', get_the_ID());
$block_1_text = get_field('block_1_text', get_the_ID());
$block_2_text_1 = get_field('block_2_text_1', get_the_ID());
$block_2_text_2 = get_field('block_2_text_2', get_the_ID());
$block_3_bg = get_field('block_3_bg', get_the_ID());
$block_3_text = get_field('block_3_text', get_the_ID());
$block_7_bg = get_field('block_7_bg', get_the_ID());
$block_6_text_1 = get_field('block_6_text_1', get_the_ID());
$block_6_text_2 = get_field('block_6_text_2', get_the_ID());

$all_images_slider = array();
$slider_gallery = get_field('slider', get_the_ID());
$slider_gallery = $slider_gallery[0]['ngg_id'];
global $nggdb;
$images_slider = $nggdb -> get_gallery($slider_gallery);
foreach ( $images_slider as $image ) {
 $all_images_slider[] = $image->imageURL;
}



$args = array( 'numberposts' =>-1, 'post_type'=>'services', 'suppress_filters' => 0, 'order' => 'ASC');
$navs = get_posts( $args );
$serv_items = array();

foreach ( $navs as $post ) :   setup_postdata( $post );

$title = $post->post_title;
$thumb = wp_get_attachment_url( get_post_thumbnail_id($icon_id) );

$serv_items[] = array('title'=> $title, 'thumb'=> $thumb);

endforeach;
wp_reset_postdata();



$args = array( 'numberposts' =>-1, 'post_type'=>'property_types', 'suppress_filters' => 0, 'order' => 'ASC');
$navs = get_posts( $args );
$type_items = array();

foreach ( $navs as $post ) :   setup_postdata( $post );

$title = $post->post_title;
$thumb = wp_get_attachment_url( get_post_thumbnail_id($icon_id) );
$row = get_field('row', get_the_ID());
$mobile_photo = get_field('mobile_photo', get_the_ID());


$all_images = array();
$all_images_2 = array();

$event_gallery = get_field('gallery', get_the_ID());
$event_gallery = $event_gallery[0]['ngg_id'];
global $nggdb;
$images = $nggdb -> get_gallery($event_gallery);
foreach ( $images as $image ) {
 $all_images_2[] = $image->imageURL;
}

$type_items[] = array('title'=> $title, 'thumb'=> $thumb, 'row'=> $row, 'mobile_photo'=> $mobile_photo
  , 'gallery'=> $all_images_2);


endforeach;
wp_reset_postdata();


$args = array( 'numberposts' =>-1, 'post_type'=>'property_types_desc', 'suppress_filters' => 0, 'order' => 'ASC');
$navs = get_posts( $args );
$type_det_items = array();

foreach ( $navs as $post ) :   setup_postdata( $post );

$title = $post->post_title;
$desc = $post->post_content;

$type_det_items[] = array('title'=> $title, 'desc'=> $desc);

endforeach;
wp_reset_postdata();



$array_menu = wp_get_nav_menu_items("home");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}

?>
<head>
<title><?= $page_title; ?></title>

<meta name="viewport" content="width=device-width,initial-scale=1.0">

<base href="<?php bloginfo('template_directory');?>/twentyseventeen"/>

<link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="assets/css/style.css" rel="stylesheet" media="screen">

<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script> -->
<script src="assets/js/jquery.flexslider-min.js" type="text/javascript"></script>
<link href="assets/css/flexslider.css" rel="stylesheet" media="screen">
<!-- <script src="http://code.jquery.com/jquery-latest.min.js"type="text/javascript"></script> -->
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js"></script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '135424837114170');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=135424837114170&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->


<script>
  fbq('track', 'ViewContent');
</script>
</head>

<script type="text/javascript">
function initMap() 
{

    var mapCanvas1 = document.getElementById('map1');
    var mapOptions1 = {
      center: new google.maps.LatLng(30.0777454,31.4259148),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map1 = new google.maps.Map(mapCanvas1, mapOptions1);
    var position_1 = {lat: 30.0777454, lng: 31.4259148};

    var marker_1 = new google.maps.Marker({
      position: position_1,
      map: map1,
      title: 'Taj City'
      // label: 'Taj City'
      // icon: $('.marker_url').val()
    });

     // marker_1.addListener('click', function() {
     //      map1.setZoom(8);
     //      map1.setCenter(marker_1.getPosition());
     //    });

}
</script>
<?php
//get_header(); 
?>
<div class="main_div">
<div id="loading">
  <img src="assets/img/spinner.gif">
</div>
<div id="loading_2">
  <img src="assets/img/spinner.gif">
</div>
  <?php 
  include 'nav_bar.php';
  ?>
  <div class="slider_div" id="home">
    <div class="flexslider carousel">
          <ul class="slides">
            <?php

            foreach ($all_images_slider as $key => $value) {
              ?>
              <li>
                <img src="<?= $value; ?>" />
              </li>

              <?php
            }
            ?>
          </ul>
        </div>
    <?php 
    //echo do_shortcode('[metaslider id=121]');

    ?>
  </div>
  <div class="slider_sep">
  </div>
  <div class="block_1_div" id="about_us">
    <img src="<?= $block_1_bg; ?>">
    <div class="block_1_div_inner">
      <p>
        <?= $block_1_text; ?>
      </p>
    </div>
  </div>
  <div class="block_2_div">
    <span class="block_2_div_spans block_2_div_span_1"><?= $block_2_text_1; ?></span>
    <span class="block_2_div_spans block_2_div_span_2"><?= $block_2_text_2; ?></span>
  </div>
  <div class="block_3_div">
    <img src="<?= $block_3_bg; ?>">
    <div class="block_3_div_inner">
      <p>
        <?= $block_3_text; ?>
      </p>
    </div>
  </div>
  <div class="blocks_bot">
    <div class="block_4_div" id="amenities">
      <span class="block_4_span_1 block_textured_span">Services & Amenities</span>
      <div id="owl-demo" class="owl-carousel owl-theme owl-demo">
        <?php 
        foreach ($serv_items as $key => $value) {
          
          ?>
              <div class="item" itemName="<?= $value['title']?>">
                <img src="<?= $value['thumb']?>"/>
                  <!-- <a href="" class="btn-4"> -->
                    <span><?= $value['title']; ?></span>
                  <!-- </a> -->
              </div>
            <?php
        }
        ?>
      </div>
    </div>
    <div class="block_5_div" id="property">
      <?php

      //echo do_shortcode('[ilightbox id="0" columns="4" class="custom_class"][/ilightbox]');

      ?>

      <div class="block_5_div_title">
        <span class="block_5_span_1 block_textured_span">Property Types</span>
      </div>
      <div class="block_5_div_inner container">
        <?php
        foreach ($type_items as $key => $value) {
          if(empty($value['row']))
          {
            ?>
            <a class="fancybox" rel="group_<?= $key; ?>" href="<?= $value['thumb'] ?>">
              <div class="block_5_div_inner_type col-sm-6">
                <span><?= $value['title'] ?></span>
                <img src="<?= $value['thumb'] ?>" class="desk_photos">
                <img src="<?= $value['mobile_photo'] ?>" class="mobile_photos">
              </div>
            </a>
            <?php
            foreach ($value['gallery'] as $key2 => $value2) {
              ?>
              <a class="fancybox" rel="group_<?= $key; ?>" href="<?= $value2 ?>"></a>
              <?php
            }
            ?>
            <?php
          }

          else
          {
            ?>
            <a class="fancybox" rel="group_<?= $key; ?>" href="<?= $value['thumb'] ?>">
              <div class="block_5_div_inner_type col-sm-12">
                <span><?= $value['title'] ?></span>
                <img src="<?= $value['thumb'] ?>" class="desk_photos">
                <img src="<?= $value['mobile_photo'] ?>" class="mobile_photos">
              </div>
            </a>
            <?php
            foreach ($value['gallery'] as $key2 => $value2) {
              ?>
              <a class="fancybox" rel="group_<?= $key; ?>" href="<?= $value2 ?>"></a>
              <?php
            }
            ?>

            <?php
          }
        }
        ?>
      </div>
      <div class="block_5_div_inner_2 container">
        <?php
        foreach ($type_det_items as $key => $value) {
          ?>

          <div class="block_5_div_inner_2_cols col-sm-4">
            <span class="block_5_div_inner_2_cols_span"><?= $value['title']; ?></span>
            <p>
              <?= $value['desc']; ?>
            </p>
          </div>

          <?php
        }
        ?>
      </div>
    </div>
  </div>
  <div class="block_2_div" id="payment">
    <span class="block_2_div_spans block_2_div_span_1"><?= $block_2_text_1; ?></span>
    <span class="block_2_div_spans block_2_div_span_2"><?= $block_2_text_2; ?></span>
  </div>
  <div class="block_7_div" style="background:url('<?= $block_7_bg; ?>')" id="contact_us">
    <div class="thank_you_div">
        <div class="thank_you_div_inner">
          <span>
            Thank you for registering!
          </span>
          <span>
            We will contact you soon.
          </span>
        </div>
      </div>
      <div class="block_7_div_inner">
        <span class="reg_div_span">Register Here</span>
          <form class="reg_form" formID="1">
          <div class="form-group">
            <input inputID="1" type="text" class="form-control form_inputs form_name" placeholder="Name" required>
          </div>
          <div class="form-group">
            <input inputID="2" type="text" class="form-control form_inputs form_mobile" placeholder="Mobile" required>
          </div>
          <div class="form-group">
            <input inputID="3" type="email" class="form-control form_inputs form_email" placeholder="Email" required>
          </div>
          <div class="form-group">
                <select inputID="4" name="units" class="form-control form_select form_inputs units" required>
                    <option value = "">Units</option>
                    <option value = "Villas">Villas</option>
                    <option value = "Apartements">Apartements</option>
                    <option value = "Duplex">Duplex</option>
                  </select>
              </div>

          <input inputID="5" type="hidden" value="<?php echo $new_url;?>" name="source" class="form_inputs">
          <button type="submit" class="btn btn-default submit_btn">Submit</button>
        </form>
      </div>
  </div>
  <div class="block_6_div" id="location">
    <span class="block_6_span_1 block_textured_span"><?= $block_6_text_1; ?></span>
    <p class="block_6_div_inner"><?= $block_6_text_2; ?></p>
    <div class="block_7">
      <div id="map1" class="maps">
      </div>
    </div>
  </div>
  <div class="footer container">
    <div class="footer_left col-sm-6">
      <span class="footer_left_span_1 footer_left_spans">A project by:</span>
      <span class="footer_left_span_2 footer_left_spans">Madinet Nasr for Housing & Development (MNHD)</span>
      <span class="footer_left_span_3 footer_left_spans">© Copyright Madinet Nasr . All Rights Reserved designed and developed by <a href="https://hugdigital.com/" target="_blank">Hug Digital</a></span>
    </div>
    <div class="footer_right col-sm-6">
      <div class="footer_right_top">
        <a href="tel:16750">
          <span class="footer_right_span">16750</span>
        </a>
        <a href="https://www.facebook.com/TajCityOfficial/" target="_blank">
          <img class="idle_img" src="assets/img/fb_icon.png">
        </a>
        <a href="https://www.instagram.com/tajcity_official/" target="_blank">
          <img class="idle_img" src="assets/img/insta_icon.png">
        </a>
      </div>
      
    </div>
  </div>
</div>

<input type="hidden" value="<?= get_permalink(249); ?>" class="thank_you_url">

<?php get_footer(); ?>

<script>

$( document ).ready(function() {

  $('.menu_span').on('click',function (e) {


    e.preventDefault();
    e.stopPropagation();

    var slideID = $(this).attr('slideID');
    slideID = slideID.split("http://");
    slideID = slideID[1];

    if(slideID == undefined)
    slideID = 'home';


      
    var post = $('#'+slideID);
    var new_height = parseInt($('.header_div').css('height'));

    var position = post.position().top - $(window).scrollTop();
    $('html, body').stop().animate({
        'scrollTop': post.offset().top - new_height
    }, 900, 'swing', function () {
        
    });

  });


  $('.all_lists').on('click',function (e) {

    var lat = parseFloat($(this).attr('lat'));
    var lng = parseFloat($(this).attr('lng'));

    calculateAndDisplayRoute(directionsService, directionsDisplay,lat,lng);

    $('html, body').animate({
        scrollTop: $(".top_div").offset().top
    }, 1000);

  });

  var owl = $(".owl-demo");
 
  owl.owlCarousel({
     
     // items : 5,
      items :3, //10 items above 1000px browser width
      itemsDesktop : [1000,3], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
      navigation : false,
      scrollPerPage : true
 
  });


  $( ".reg_form" ).submit(function( event ) {
    
    event.preventDefault();
    return false;

    $('#loading').show();

    var formID = $(this).attr('formID');
    var inputs = $(this).find('.form_inputs');

    $.each( inputs, function( key, value ) {
      
      inputID = $(value).attr('inputID');
      var input_old = $(value).val();
      $('#input_' + formID + '_' + inputID).val(input_old);


    });

    $('#gform_' + formID).submit();

    // var name = $('.form_name').val();
    // var mobile = $('.form_mobile').val();
    // var email = $('.form_email').val();

        jQuery(document).bind('gform_post_render', function(){
   

    var url = $('.thank_you_url').val();
    window.location.href = url;

  });
  

  });

});

$(window).scroll(function() {



});

window.onload = function() {

initMap();

$('.footer_right').css('height', $('.footer_left').css('height'));

$(".fancybox").fancybox();

$('.flexslider').flexslider({
        animation: "fade",
        controlNav: false,
        directionNav: false,
        smoothHeight: true,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });

$('#loading_2').hide();

}

$(window).on('resize', function()
{

$('.footer_right').css('height', $('.footer_left').css('height'));


});



</script>

<script type="text/javascript" src="assets/owl_carousel_assets/owl.carousel.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCe8ySOOlQeodi3WENjiXmopN24CvJEsto&libraries=geometry"></script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>